'''
Created on Jun 10, 2015

@author: Minh Ngoc Le
'''
from theano import tensor as T 
import theano

import numpy as np
import logging
from logging.config import fileConfig

floatX = theano.config.floatX
_log = logging.getLogger('rlsrl')


class MaxEntNet(object):
    '''
    TODO: Give it an appropriate name
    '''

    def __init__(self, embedding_specs, block2embedding, hidden_dims, 
                 output_dims, trainable_embeddings=None, 
                 bg_index=1, beta=0.2, beta_training=False, exponent=3,
                 rng=np.random.RandomState()):
        '''
        Constructor
        '''
        self.block2embedding = block2embedding
        self.alpha = theano.shared(np.cast[floatX](0.1), 'learning_rate')
        self.bg_index = bg_index
        if trainable_embeddings is None:
            trainable_embeddings = embedding_specs.keys()
        trainable_embeddings = set(trainable_embeddings)
        if beta_training:
            raise ValueError("Unsupported")
        assert hidden_dims == 0
        
        randm = lambda val, *dims: rng.uniform(low=-val, high=val, size=dims) # random matrix
        path2value = {}
        uniq_paths = set(p for p in embedding_specs.values() if isinstance(p, basestring))
        for p in uniq_paths:
            _log.info("Loading pre-trained embeddings from %s..." %p)
            path2value[p] = np.asarray(np.load(p), dtype=floatX)
            _log.info("Loading pre-trained embeddings from %s... Done." %p)
        E = {}
        for name, spec in embedding_specs.items():
            E_values = np.asarray(path2value[spec] if isinstance(spec, basestring)
                                else randm(0.1, *spec), dtype=floatX)
            E[name] = theano.shared(E_values, 'E_%s' %name)
        trainable_E = ([E[key] for key in trainable_embeddings if key in E])
        self.embeddings = E
        embedding_dims = sum([E[e].get_value().shape[1] for e in block2embedding])
        W_values = np.asarray(randm(0.1, output_dims, embedding_dims), dtype=floatX)
        W = theano.shared(W_values, 'W')
#         b_values = np.asarray(randm(0.1, output_dims), dtype=floatX)
#         b = theano.shared(b_values, 'b')

        x = T.matrix('x', 'int32')
        y_hat = T.matrix('y_hat', 'int32')
        z = T.concatenate([E[e][x[:,i]] for i, e in enumerate(block2embedding)], axis=1)
        z.name = 'z'
        y = T.nnet.softmax(T.dot(z, W.T))
        self.params = (trainable_E + [W])
#         y = T.nnet.softmax(T.dot(z, W.T)+b.dimshuffle('x',0))
#         self.params = (trainable_E + [W, b])
        reg = 0.00001 * sum(T.sum(T.sqr(e)) for e in self.params)
        cost = T.mean(-T.log(y[T.arange(y_hat.shape[0]), y_hat[:,0]])) + reg
        
        grad = T.grad(cost, self.params)
        _log.info("Compiling functions...")
        self.fprop = theano.function([x], y)
        self.hidden = lambda x: None
        self.regularization = theano.function([], reg)
        self.cost = theano.function([x, y_hat], cost)
        self.grad = theano.function([x, y_hat], grad)
        
        self.update = theano.function([x, y_hat], grad,
                                      updates=[(p, p-T.clip(self.alpha*g, T.cast(-0.5/p.shape[-1]**0.5, floatX), 
                                                                        T.cast(0.5/p.shape[-1]**0.5, floatX)))
                                               for p, g in zip(self.params, grad)])
#                                     , mode=theano.compile.MonitorMode(
#                                              post_func=self.detect_nan))
        _log.info("Compiling functions... Done.")


class CubeNet(object):
    '''
    TODO: Give it an appropriate name
    '''

    def __init__(self, embedding_specs, block2embedding, hidden_dims, 
                 output_dims, trainable_embeddings=None, 
                 bg_index=1, beta=None, beta_training=False,
                 rng=np.random.RandomState()):
        '''
        Constructor
        '''
        assert beta is None # not used
        assert not beta_training # not supported
        self.block2embedding = block2embedding
        self.alpha = theano.shared(np.cast[floatX](0.1), 'learning_rate')
        self.bg_index = bg_index
        if trainable_embeddings is None:
            trainable_embeddings = range(len(embedding_specs))
        
        randm = lambda val, *dims: rng.uniform(low=-val, high=val, size=dims)  # random matrix
        path2value = {}
        uniq_paths = set(p for p in embedding_specs if isinstance(p, basestring))
        for p in uniq_paths:
            _log.info("Loading pre-trained embeddings from %s..." %p)
            path2value[p] = np.asarray(np.load(p), dtype=floatX)
            _log.info("Loading pre-trained embeddings from %s... Done." %p)
        E = []
        for index, spec in enumerate(embedding_specs):
            E_values = np.asarray(path2value[spec] if isinstance(spec, basestring)
                                  else randm(0.1, *spec), dtype=floatX)
            E.append(theano.shared(E_values, 'E%d' %index))
        trainable_E = [E[i] for i in trainable_embeddings]
        embedding_dims = sum([E[e].get_value().shape[1] for e in block2embedding.values()])
        W0_values = np.asarray(randm(0.1, hidden_dims, embedding_dims), dtype=floatX)
        W0 = theano.shared(W0_values, 'W0')
        b0_values = np.asarray(randm(0.1, hidden_dims), dtype=floatX)
        b0 = theano.shared(b0_values, 'b0')
        W1_values = np.asarray(randm(0.1, output_dims, hidden_dims), dtype=floatX)
        W1 = theano.shared(W1_values, 'W1')
        b1_values = np.asarray(randm(0.1, output_dims), dtype=floatX)
        b1 = theano.shared(b1_values, 'b1')
        
        x = T.matrix('x', 'int32')
        y_hat = T.matrix('y_hat', 'int32')
        z = T.concatenate([E[e][x[:,b]] for b, e in block2embedding.items()], axis=1)
        z.name = 'z_hidden'
        hidden = (T.dot(z, W0.T) + b0.dimshuffle('x', 0))**3
        hidden.name = 'hidden'
        y = T.nnet.softmax(T.dot(hidden, W1.T) + b1.dimshuffle('x', 0))
        self.params = trainable_E + [W0, b0, W1, b1]
        reg = 0.00000001 * sum([T.sum(T.sqr(p)) for p in self.params])
        cost = T.mean(-T.log(y[T.arange(y_hat.shape[0]), y_hat[:,0]])) + reg
        
        grad = T.grad(cost, self.params)
        _log.info("Compiling functions...")
        self.fprop = theano.function([x], y)
        self.hidden = theano.function([x], hidden)
        self.regularization = theano.function([], reg)
        self.cost = theano.function([x, y_hat], cost)
        self.grad = theano.function([x, y_hat], grad)
        
        self.update = theano.function([x, y_hat], grad,
                                      updates=[(p, p-T.clip(self.alpha*g, 
                                                            T.cast(-0.5/p.shape[-1]**0.5, floatX), 
                                                            T.cast(0.5/p.shape[-1]**0.5, floatX)))
                                               for p, g in zip(self.params, grad)])
#                                     , mode=theano.compile.MonitorMode(
#                                              post_func=self.detect_nan))
        _log.info("Compiling functions... Done.")


def validate_batch(model, batch_x, batch_y):
    assert batch_x.shape[1] == len(model.block2embedding)
    assert batch_y.shape[1] == 1


def monitor(model, batch_x, batch_y, ds_name, batch_size):
    validate_batch(model, batch_x, batch_y)
    batch_y = batch_y[:batch_size] 
    batch_x = batch_x[:batch_size]
    y = model.fprop(batch_x)
    y_index = np.argmax(y, axis=1).reshape((-1, 1))
    _log.info("\t[%s] Cost: %f" %(ds_name, model.cost(batch_x, batch_y)))
#     h = np.sum(model.hidden(batch_x), axis=1)
#     _log.info("\t[%s] Hidden activation: %f (std: %f)" %(ds_name, np.mean(h), np.std(h)))
    _log.info("\t[%s] Regularization term: %f" %(ds_name, model.regularization()))
    _log.info("\t[%s] Num. types: %d" %(ds_name, len(np.unique(y_index))))
    _log.info("\t[%s] NIL vs. non-NIL accuracy: %f" 
              %(ds_name, np.mean(np.equal(y_index==model.bg_index, batch_y==model.bg_index))))
    predicted_total = np.sum(y_index != model.bg_index)
    gold_total = np.sum(batch_y != model.bg_index)
    correct = np.sum(np.logical_and(y_index != model.bg_index, y_index == batch_y))
    _log.info("\t[%s] Precision: %f" %(ds_name, float(correct)/predicted_total 
                                       if predicted_total > 0 else float('nan')))
    _log.info("\t[%s] Recall: %f" %(ds_name, float(correct)/gold_total
                                    if gold_total > 0 else float('nan')))
    _log.info("\t[%s] Gradient norm:" %ds_name)
    norm = lambda x: np.linalg.norm(x, axis=-1) if len(x.shape) >= 2 else np.absolute(x)
    for param, grad in zip(model.params, model.grad(batch_x, batch_y)):
        grad_norms = norm(grad)
        _log.info("\t\t%s: %f (std: %f, max: %f, min: %f)" 
                  %(param.name, np.mean(grad_norms),
                    np.std(grad_norms), np.max(grad_norms), np.min(grad_norms)))
    _log.info("\t[%s] Parameter norms:" %ds_name)
    for param in model.params:
        param_norms = norm(param.get_value())
        _log.info("\t\t%s: %f (std: %f, max: %f, min: %f)" 
                  %(param.name, np.mean(param_norms),
                    np.std(param_norms), np.max(param_norms), np.min(param_norms)))


def detect_nan(self, i, node, fn):
    for output in fn.outputs:
        if (not isinstance(output[0], np.random.RandomState) and
            np.isnan(output[0]).any()):
            print '*** NaN detected ***'
            
            for p in self.params:
                print p.name, np.sum(p.get_value())

            theano.printing.debugprint(node)
            print 'Inputs : %s' % [input[0] for input in fn.inputs]
            print 'Outputs: %s' % [output[0] for output in fn.outputs]
            break


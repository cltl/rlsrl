'''
Created on May 29, 2015

@author: Minh Ngoc Le
'''
import pickle
import re
import sys
from traceback import print_exc

from config import *
from data import Vocabulary, parse_conll_sentence, save_vocab
from gensim.models.word2vec import Word2Vec
import numpy as np


if __name__ == '__main__':
    words = Vocabulary()
    pos = Vocabulary()
    rels = Vocabulary()
    frames = Vocabulary()
    roles = Vocabulary()
    actions = Vocabulary()
    actions.get_index('__NIL__') # reserve action #1
    # iterate sentences
    sys.stderr.write("Processing corpus in %s...\n" %original_data_dir)
    for rids, raw_file, sent_file in ((train_rids, train_raw_file, train_file),
                                      (valid_rids, valid_raw_file, valid_file),
                                      (test_rids, test_raw_file, test_file)):
        sents = []
        with open(raw_file, 'w') as raw_out:  
            for fname in os.listdir(original_data_dir):
                path = os.path.join(original_data_dir, fname) 
                with open(path) as f: # because Python 2.6 doesn't allow multiple files here
                    line = f.readline()
                    while line:
                        m = re.match("# RID: (\d+)", line)
                        if m:
                            rid = int(m.group(1))
                            try:
                                skipped = False
                                lines = []
                                line = f.readline()
                                while line:
                                    if line == '\n': break
                                    if line[0] != '#':
                                        lines.append(line)
                                    if line.strip() == 'SKIPPED':
                                        skipped = True
                                        break
                                    line = f.readline()
                                if not skipped:
                                    s = parse_conll_sentence(lines, rid, words, pos, rels, 
                                                             frames, roles, actions)
                                    raw = ''.join(lines) + "\n"
                                    if rid in rids:
                                        raw_out.write(raw)
                                        sents.append(s)
                            except:
                                sys.stderr.write("Error at file: %s, sentence: %d\n" %(fname, rid))
                                print_exc()
                        line = f.readline()
        sys.stderr.write("Wrote raw sentences to %s.\n" %raw_file)
        with open(sent_file, 'w') as sent_out:
            pickle.dump(sents, sent_out)
            sys.stderr.write("Wrote %d sentences to %s.\n" %(len(sents), sent_file))
    sys.stderr.write("Done.\n")
    save_vocab(words, features["word"]["vocab_file"])
    save_vocab(pos, features["pos"]["vocab_file"])
    save_vocab(rels, features["rel"]["vocab_file"])
    save_vocab(frames, features["frame"]["vocab_file"])
    save_vocab(roles, features["role"]["vocab_file"])
    save_vocab(actions, features["action"]["vocab_file"])
    # extract pretrained embeddings
    sys.stderr.write("Reading word2vec model from %s... " %word2vec_path)
    w2v = Word2Vec.load_word2vec_format(word2vec_path, binary=True, norm_only=True)
    sys.stderr.write("Done.\n")
    sys.stderr.write("Extracting pre-trained word embeddings... ")
    embeddings = np.zeros((len(words.word2index), w2v.layer1_size))
    pretrain_count = 0
    for word in words.word2index:
        if word in w2v:
            embeddings[words.get_index(word)] = w2v[word]
            pretrain_count += 1
    sys.stderr.write("%d extracted.\n" %pretrain_count)
    np.save(features["word"]["embedding_file"], embeddings)
    sys.stderr.write("Wrote word embeddings (%d by %d) to %s.\n" 
                     %(embeddings.shape[0], embeddings.shape[1], 
                       features["word"]["embedding_file"]))

'''
Created on Jun 17, 2015

@author: Minh Ngoc Le
'''
from collections import defaultdict
import pickle
import sys
import numpy as np
import logging


_log = logging.getLogger('rlsrl')


class Sentence(object):
    
    def __init__(self, sid):
        self.sid = sid
        self.words = []
        self.pos = []
        self.text = ['']
        self.heads = {}
        self.syn_links = defaultdict(dict)
        self.frames = defaultdict(dict)
        self.roles = defaultdict(dict)
        self.actions = defaultdict(dict)
        self.sem_links = [] # because dictionaries will cause loss of data
    
    
class Vocabulary(object):
    
    def __init__(self):
        self.word2index = {'__MISSING__': 0}
        
    def get_index(self, word):
        if word not in self.word2index:
            self.word2index[word] = len(self.word2index)
        return self.word2index[word]

    def add_all(self, words):
        for word in words:
            self.word2index[word] = len(self.word2index)


def parse_conll_sentence(lines, rid="", words=Vocabulary(), pos=Vocabulary(), 
                         rels=Vocabulary(), frames=Vocabulary(), 
                         roles=Vocabulary(), actions=Vocabulary()):
    sent = Sentence(rid)
    sent.words.append(words.get_index('__ROOT__'))
    sent.pos.append(pos.get_index('__ROOT__'))
    predicates = {}
    arguments = defaultdict(list)
    for line in lines:
        fields = line.strip().split('\t')
        wid = int(fields[0])
        sent.text.append(fields[1])
        sent.words.append(words.get_index(fields[1]))
        sent.pos.append(pos.get_index(fields[4]))
        if fields[8] != '_':
            head_id = int(fields[8])
            sent.heads[wid] = head_id
            sent.syn_links[wid][head_id] = rels.get_index(fields[9] + "#UP")
            sent.syn_links[head_id][wid] = rels.get_index(fields[9] + "#DOWN")
        for i in range(10, len(fields), 2):
            if fields[i] != '_': predicates[i] = (wid, fields[i])
            if fields[i+1] != '_': arguments[i].append((wid, fields[i+1]))
    for key in predicates:
        pred_id, frame = predicates[key]
        sent.sem_links.append((pred_id, 0, frame))
        for arg_id, role in arguments[key]:
            if arg_id in sent.actions[pred_id]:
                _log.warn("Clash at sentence #%s, predicate %d, argument %d" 
                          %(rid, pred_id, arg_id))
            sent.frames[pred_id][arg_id] = frames.get_index(frame)
            sent.roles[pred_id][arg_id] = roles.get_index(role)
            action = actions.get_index(frame + '#' + role)
            sent.actions[pred_id][arg_id] = action
            sent.sem_links.append((pred_id, arg_id, role))
    return sent


def save_vocab(vocab, path):
    with open(path, 'w') as f:
        pickle.dump(vocab.word2index, f)
        sys.stderr.write("Wrote %d items into %s.\n" %(len(vocab.word2index), path))


class DatasetPermutation(object):
    def __init__(self, x, y, rng):
        self.x = x
        self.y = y
        self.rng = rng
        
    def __call__(self):
        indices = self.rng.permutation(len(self.x))
        return self.x[indices], self.y[indices]
        

class DatasetSample(object):
    def __init__(self, x, y, rng, nil_id=1):
        self.x = x
        self.y = y
        self.rng = rng
        counts = np.bincount(y[:,0])
        first, second = counts[np.argsort(-counts)[:2]]
        self.bg_start = len(y)-first
        self.bg_stop = self.bg_start + second
        self.indices = np.argsort(y[:,0] == nil_id)

    def __call__(self):
        self.rng.shuffle(self.indices[self.bg_start:])
        sampled_indices = np.copy(self.indices[:self.bg_stop])
        self.rng.shuffle(sampled_indices)
        _log.info("Sampled %d from %d data points." %(len(sampled_indices), len(self.y)))
        return self.x[sampled_indices], self.y[sampled_indices]
        


if __name__ == '__main__':
    pass
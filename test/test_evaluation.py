'''
Created on Jul 13, 2015

@author: Minh Ngoc Le
'''
import os
from data import parse_conll_sentence
from evaluation import evaluate

sent_path = os.path.dirname(__file__)
with open(os.path.join(sent_path, 'sentence01.conll')) as f:
    sent01 = parse_conll_sentence(f.readlines())
with open(os.path.join(sent_path, 'sentence02_clash.conll')) as f:
    sent02 = parse_conll_sentence(f.readlines())

def test_simple():
    linkage = [(2, 2, 'Subtype'), (2, 0, 'WrongFrame')]
    p, r, f1, correct, gold_total, predicted_total = evaluate([sent01], [linkage])
    assert predicted_total == 1
    assert gold_total == 5
    assert correct == 1
    print (p, r, f1)

def test_predicate_disambiguation1():
    linkage = [(2, 2, 'Subtype'), (2, 0, 'WrongFrame')]
    p, r, f1, correct, gold_total, predicted_total = evaluate([sent01], [linkage], 
                                                              predicate_disambiguation=True)
    assert predicted_total == 2
    assert gold_total == 8
    assert correct == 1
    print (p, r, f1)
    
def test_predicate_disambiguation2():
    linkage = [(2, 2, 'Subtype'), (2, 0, 'Type')]
    p, r, f1, correct, gold_total, predicted_total = evaluate([sent01], [linkage], 
                                                              predicate_disambiguation=True)
    assert predicted_total == 2
    assert gold_total == 8
    assert correct == 2
    print (p, r, f1)
    
def test_clash():
    p, r, f1, correct, gold_total, predicted_total = evaluate([sent02], [[(1, 1, 1)]])
    assert predicted_total == 1
    assert gold_total == 5
    assert correct < 1
    print (p, r, f1)
'''
Created on Jul 13, 2015

@author: Minh Ngoc Le
'''
import os
from data import parse_conll_sentence
import local
from local import identify_and_classify_arguments, classify_arguments
import numpy as np
from feature import chain

sent_path = os.path.join(os.path.dirname(__file__), 'sentence01.conll')
with open(sent_path) as f:
    sent = parse_conll_sentence(f.readlines())

class MockModel(object):
    def fprop(self, x):
        return np.random.rand(len(x), len(local.actions))

def test_identify_and_classify_arguments():
    links = identify_and_classify_arguments(MockModel(), chain(), [sent])[0]
    print links
    assert len(links) > len(sent.sem_links)

def test_classify_arguments():
    links = classify_arguments(MockModel(), chain(), [sent])[0]
    print links
    gold_pred_arg_links = [l for l in sent.sem_links if l[1] > 0]
    assert len(links) == len(gold_pred_arg_links)

def test_features():
    for f, actions in local.frame2actions.items():
        assert isinstance(f, basestring)
        assert len(actions) > 0

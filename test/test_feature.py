'''
Created on Jul 14, 2015

@author: Minh Ngoc Le
'''
import os
from data import parse_conll_sentence
from feature import Position, RolePOS, PredicatePOS

sent_path = os.path.join(os.path.dirname(__file__), 'sentence01.conll')
with open(sent_path) as f:
    sent = parse_conll_sentence(f.readlines())

def test_position():
    p = Position()
    assert p(sent, 1, 2) == 0
    assert p(sent, 3, 2) == 1
    assert p(sent, 2, 2) == 2
    
def test_pos():
    rp = RolePOS()
    assert rp(sent, 1, 2) > 0
    pp = PredicatePOS()
    assert pp(sent, 1, 2) > 0
    assert rp(sent, 1, 2) != pp(sent, 1, 2)
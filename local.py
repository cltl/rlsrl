'''
Created on May 29, 2015

@author: Minh Ngoc Le
'''
import logging
import pickle
from time import time

import theano

from config import *
from feature import chain, RoleWord, PredicateWord
from models import monitor, CubeNet
import numpy as np
import tsne
from data import DatasetPermutation, DatasetSample
from evaluation import evaluate


_log = logging.getLogger('rlsrl')


with open(features["action"]["vocab_file"]) as f:
    actions = pickle.load(f)
    index2action = dict((v, k) for k, v in actions.items())
    nil_id = actions['__NIL__']
with open(features["frame"]["vocab_file"]) as f:
    frames = pickle.load(f)
    index2frame = dict((v, k) for k, v in frames.items())
with open(features["role"]["vocab_file"]) as f:
    roles = pickle.load(f)
    index2role = dict((v, k) for k, v in roles.items())
frame2actions = dict()
for a, i in actions.items():
    if i > 0 and i != nil_id:
        f, _ = a.split('#')
        if f not in frame2actions:
            frame2actions[f] = []
        frame2actions[f].append(i)

def load_sentences(path):
    with open(path) as f:
        _log.info("Loading sentences from %s..." %path)
        sents = pickle.load(f)
        _log.info("Loading sentences... Done.")
    return sents


def iter_solution(model, x):
    batch_size = 10000
    for i in xrange(0, len(x), batch_size):
        xb = x[i:i+batch_size]
        yb = model.fprop(xb)
        for y_i in yb:
            yield y_i


def classify_arguments(model, feature_extractor, sents):
    _log.info("Start decoding %s sentences" %len(sents))
    x, _ = build_dataset(sents, feature_extractor, nil_id)
    y_hat_iter = iter_solution(model, x)
    linkages = []
    for sid, sent in enumerate(sents):
        if sid % 10 == 0:
            _log.info("Sentence #%d" %sid)
        linkage = set()
        for i in range(1, len(sent.words)):
            for j in range(1, len(sent.words)):
                y_ij = next(y_hat_iter)
                if j in sent.frames[i]: # identification is done
                    known_frame = index2frame[sent.frames[i][j]]
                    valid_actions = frame2actions[known_frame]
                    a = valid_actions[np.argmax(y_ij[valid_actions])]
                    f, r = index2action[a].split('#')
                    assert f == known_frame
                    linkage.add((i, j, r))
        linkages.append(list(linkage))
    _log.info("Finished decoding with %s linkages" %len(linkages))
    return linkages


def identify_and_classify_arguments(model, feature_extractor, sents):
    _log.info("Start decoding %s sentences" %len(sents))
    x, _ = build_dataset(sents, feature_extractor, nil_id)
    y_hat_iter = iter_solution(model, x)
    linkages = []
    for sid, sent in enumerate(sents):
        if sid % 10 == 0:
            _log.info("Sentence #%d" %sid)
        linkage = set()
        for i in range(1, len(sent.words)):
            known_frame_ids = set(sent.frames[i].values())
            for j in range(1, len(sent.words)):
                y_ij = next(y_hat_iter)
                if not known_frame_ids: 
                    # ignore the pair but we still need to iterate the results
                    # otherwise everything will be shifted
                    continue
                best_role = '__NIL__'
                best_score = y_ij[nil_id]
                for known_frame_id in known_frame_ids:
                    valid_actions = frame2actions[index2frame[known_frame_id]]
                    a = valid_actions[np.argmax(y_ij[valid_actions])]
                    f, r = index2action[a].split('#')
                    assert f == index2frame[known_frame_id]
                    if y_ij[a] > best_score:
                        best_score = y_ij[a]
                        best_role = r
                if best_role != '__NIL__':
                    linkage.add((i, j, best_role))
        linkages.append(list(linkage))
    try: # make sure things aren't shifted
        next(y_hat_iter)
        raise ValueError("Results weren't fully consumed, something must be wrong")
    except StopIteration:
        pass
    _log.info("Finished decoding with %s linkages" %len(linkages))
    return linkages


def build_dataset(sents, feature_extractor, nil_id): 
    _log.info("Building dataset...")
    x = []
    y = []
    for sent in sents:
        for i in range(1, len(sent.words)):
            for j in range(1, len(sent.words)):
                x.append(feature_extractor(sent, i, j))
                y.append((sent.actions[i][j] 
                          if i in sent.actions and j in sent.actions[i]
                          else nil_id,))
    x = np.array(x, dtype='int32')
    y = np.array(y, dtype='int32')
    _log.info("Building dataset... Done (%d instances)." %len(x))
    return x, y


def train(model, train_ds, valid_ds, batch_size, num_epochs, 
          first_alpha, last_alpha, saturate_epoch):
    _log.info("Training...")
    start = time()
    model.alpha.set_value(np.cast[theano.config.floatX](first_alpha))
    for epoch in xrange(num_epochs):
        x, y = train_ds()
        if epoch % 1 == 0:
            _log.info("Epoch %d:" %epoch)
            valid_x, valid_y = valid_ds()
            monitor(model, x, y, "train", batch_size)
            monitor(model, valid_x, valid_y, "valid", batch_size)
            _log.info("\tLearning rate: %f" %model.alpha.get_value())
            _log.info("\tSeconds since start: %f" %(time()-start))
        if epoch <= saturate_epoch:
            new_alpha = first_alpha+(last_alpha-first_alpha)*float(epoch)/saturate_epoch
            model.alpha.set_value(np.cast[theano.config.floatX](new_alpha))
        for i in xrange(0, len(y), batch_size):
            model.update(x[i:i+batch_size], y[i:i+batch_size])


def run(feature_extractor=chain(RoleWord, PredicateWord),
        pretrained_embeddings=set(), trainable_embeddings=features.keys(),
        model_type=CubeNet, hidden_dims=300, beta=0.2,
        batch_size=1024, num_epochs=100, resample=True,
        first_alpha = 0.01, last_alpha = 0.001, saturate_epoch=50, 
        name='unnamed', visualize=False, sample_output=False):
    _log.info("Running local model %s (pretrained_embeddings=%s, trainable_embeddings=%s, "
              "model_type=%s, hidden_dims=%d, num_epochs=%d, beta=%f, " 
              "first_alpha=%.4f, last_alpha=%.4f, saturate_epoch=%d)" 
              %(name, pretrained_embeddings, trainable_embeddings, 
                model_type.__name__, hidden_dims, num_epochs, 
                beta, first_alpha, last_alpha, saturate_epoch))
    # model
    specs = dict((name, (features[name]["labels"], features[name]["dims"])) 
                 for name in feature_extractor.type())
    for key in pretrained_embeddings:
        specs[key] = features[key]["embedding_file"]
    model = model_type(specs, feature_extractor.type(), 
                       hidden_dims, features["action"]["labels"], 
                       trainable_embeddings=trainable_embeddings,
                       bg_index=nil_id, rng=rng, beta=beta)
    _log.info("Building batches...")
    x, y = build_dataset(load_sentences(train_file), feature_extractor, nil_id)
    valid_x, valid_y = build_dataset(load_sentences(valid_file), feature_extractor, nil_id)
    if resample:
        train_ds = DatasetSample(x, y, rng, nil_id)
        valid_ds = DatasetSample(valid_x, valid_y, rng, nil_id)
    else:
        train_ds = DatasetPermutation(x, y, rng)
        valid_ds = DatasetPermutation(valid_x, valid_y, rng)
    train(model, train_ds, valid_ds, batch_size, num_epochs, 
          first_alpha, last_alpha, saturate_epoch)
    _log.info("Evaluation on test set:")
    sents = load_sentences(test_file)
    linkages = identify_and_classify_arguments(model, feature_extractor, sents)
    p, r, f1, correct, gold_total, predicted_total = evaluate(sents, linkages, predicate_disambiguation=False)
    _log.info("P=%.4f, R=%.4f, F1=%.4f" %(p, r, f1))
    _log.info("#correct=%d, #gold=%d, #predicted=%d" %(correct, gold_total, predicted_total))
    if sample_output:
        sample_output_path = '%s.out' %name
        with open(sample_output_path, 'w') as f:
            for linkage, sent in zip(linkages, sents):
                f.write('%d\n' %sent.sid) 
                f.write(' '.join(sent.text[1:]) + '\n')
                for i, j, l in linkage:
                    if j == 0:
                        f.write("<predicate>\t%s(%d)\t%s(%d)\n" %(sent.text[i], i,
                                                                  l, frames[l]))
                    else:
                        f.write("%s(%d)\t%s(%d)\t%s(%d)\n" %(sent.text[i], i,
                                                             sent.text[j], j,
                                                             l, roles[l]))
                f.write('\n')
        _log.info("Sample output wrote to file: %s" %sample_output_path)
    if visualize:
        for fname, E in model.embeddings.items():
            limit = 10000
            if "vocab" not in features[fname]:
                with open(features[fname]["vocab_file"]) as f:
                    features[fname]["vocab"] = pickle.load(f)
            vocab = features[fname]["vocab"]
            tnse_output = "%s-embeddings-%s.png" %(name, fname)
            tsne.visualize(E.get_value()[:limit], vocab, figure_size=features[fname]["figure_size"], output=tnse_output)
            _log.info("Embedding set #%s (first %d rows only) was written to %s" %(fname, limit, tnse_output))
            
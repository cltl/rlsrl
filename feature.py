'''
Created on Jun 15, 2015

@author: Minh Ngoc Le
'''
import logging

_log = logging.getLogger('rlsrl')

class FeatureExtractor(object):
    def __call__(self, sent, i, j):
        raise NotImplementedError
    def type(self):
        raise NotImplementedError

class RoleWord(FeatureExtractor):
    def __call__(self, sent, i, j):
        return sent.words[i]
    def type(self):
        return "word"
    
class PredicateWord(FeatureExtractor):
    def __call__(self, sent, i, j):
        return sent.words[j]
    def type(self):
        return "word"
    
class RoleHeadWord(FeatureExtractor):
    def __call__(self, sent, i, j):
        if i in sent.heads:
            return sent.words[sent.heads[i]]
        return 0
    def type(self):
        return "word"
    
class PredicateHeadWord(FeatureExtractor):
    def __call__(self, sent, i, j):
        if j in sent.heads:
            return sent.words[sent.heads[j]]
        return 0
    def type(self):
        return "word"
    
class RoleHeadRel(FeatureExtractor):
    def __call__(self, sent, i, j):
        if i in sent.heads:
            return sent.syn_links[i][sent.heads[i]]
        return 0
    def type(self):
        return "rel"
    
class PredicateHeadRel(FeatureExtractor):
    def __call__(self, sent, i, j):
        if j in sent.heads:
            return sent.syn_links[j][sent.heads[j]]
        return 0
    def type(self):
        return "rel"

class RolePOS(FeatureExtractor):
    def __call__(self, sent, i, j):
        return sent.pos[i]
    def type(self):
        return "pos"
    
class PredicatePOS(FeatureExtractor):
    def __call__(self, sent, i, j):
        return sent.pos[j]
    def type(self):
        return "pos"
        
class Position(FeatureExtractor):
    def __call__(self, sent, i, j):
        return ('LEFT' if i < j else 
                'RIGHT' if i > j else
                'SAME')
    def type(self):
        return "position"
    
class ChainedFeatureExtractor(FeatureExtractor):
    def __init__(self, *extractors):
        extractors = list(extractors)
        for i in range(len(extractors)):
            if issubclass(extractors[i], FeatureExtractor):
                extractors[i] = extractors[i]()
        self.extractors = extractors
        
    def __call__(self, sent, i, j):
        return [e(sent, i, j) for e in self.extractors]
    
    def type(self):
        return [e.type() for e in self.extractors]

def chain(*extractors):
    return ChainedFeatureExtractor(*extractors)

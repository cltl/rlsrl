'''
Created on May 29, 2015

@author: Minh Ngoc Le
'''
from logging.config import fileConfig
import os
import numpy


home_dir = os.path.dirname(__file__)

original_data_dir = os.path.join(home_dir, 'bauer2014')
data_dir = os.path.join(home_dir, 'data')
train_rids = set(range(1, 5501))
valid_rids = set(range(5501, 5701))
test_rids = set(range(5501, 6000))
train_raw_file = os.path.join(data_dir, 'train.conll')
valid_raw_file = os.path.join(data_dir, 'valid.conll')
test_raw_file = os.path.join(data_dir, 'test.conll')
train_file = os.path.join(data_dir, 'train.pkl')
valid_file = os.path.join(data_dir, 'valid.pkl')
test_file = os.path.join(data_dir, 'test.pkl')

features = {"word": {"vocab_file": os.path.join(data_dir, 'words.pkl'),
                     "embedding_file": os.path.join(data_dir, 'word_embeddings.npy'),
                     "labels": 100000,
                     "dims": 50,
                     "figure_size": (100, 100),
                     },
            "pos": {"vocab_file": os.path.join(data_dir, 'pos.pkl'),
                    "labels": 100,
                    "dims": 5,
                    "figure_size": (10, 10)},
            "rel": {"vocab_file": os.path.join(data_dir, 'rels.pkl'),
                    "labels": 400,
                    "dims": 8,
                    "figure_size": (20, 20)},
            "position": {"vocab": dict((i, i) for i in range(3)),
                         "labels": 3, # before, on, after
                         "dims": 2,
                         "figure_size": (2, 2)},
            "frame": {"vocab_file": os.path.join(data_dir, 'frames.pkl'),
                    "labels": 5000,
                    "dims": 10},
            "role": {"vocab_file": os.path.join(data_dir, 'roles.pkl'),
                    "labels": 5000,
                    "dims": 10}, 
            "action": {"vocab_file": os.path.join(data_dir, 'actions.pkl'),
                       "labels": 9640, # the maximal number of actions, not all of them may be used
                       "dims": 10},
            }

rand_seed = 20150530
rng = numpy.random.RandomState(rand_seed)

# word2vec_path = '/home/minh/scistor/GoogleNews-vectors-negative300.bin'
word2vec_path = '/home/minhle/scratch/GoogleNews-vectors-negative300.bin'
assert os.path.exists(word2vec_path)

fileConfig(os.path.join(home_dir, 'logging.cfg'))
'''
Created on Jun 17, 2015

@author: Minh Ngoc Le
'''
from feature import chain, RoleWord, PredicateWord, RoleHeadWord, RoleHeadRel, \
    PredicateHeadWord, PredicateHeadRel, RolePOS, PredicatePOS, Position
import local
from models import MaxEntNet, CubeNet


if __name__ == '__main__':
    feature_extractors = chain(RoleWord, PredicateWord, RoleHeadWord, RoleHeadRel, PredicateHeadWord, PredicateHeadRel, RolePOS, PredicatePOS, Position)
    local.run(name="MaxEntNet-pretrainedEmbedding", feature_extractor=feature_extractors, pretrained_embeddings=("word",), model_type=MaxEntNet, resample=False, hidden_dims=0, first_alpha=0.1, last_alpha=0.1, saturate_epoch=50, num_epochs=2, visualize=True, sample_output=True)
#     local.run(name="MaxEntNet", feature_extractor=feature_extractors, model_type=MaxEntNet, resample=True, hidden_dims=0, first_alpha=0.1, last_alpha=0.01, saturate_epoch=350, num_epochs=400, visualize=True, sample_output=True)
#     local.run(name="CubeNet-pretrainedEmbedding", feature_extractor=feature_extractors, pretrained_embeddings=("word",), model_type=CubeNet, resample=True, hidden_dims=0, first_alpha=0.1, last_alpha=0.01, saturate_epoch=350, num_epochs=400, visualize=True, sample_output=True)
#     local.run(name="CubeNet", feature_extractor=feature_extractors, model_type=CubeNet, resample=True, hidden_dims=0, first_alpha=0.1, last_alpha=0.01, saturate_epoch=350, num_epochs=400, visualize=True, sample_output=True)
    
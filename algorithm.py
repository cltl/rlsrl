'''
Created on May 12, 2015

@author: Minh Ngoc Le
'''

class AlgorithmBase(object):
    
    def train(self):
        raise NotImplemented
    
    def train_batch(self):
        raise NotImplemented
    

class PolicyGradient(AlgorithmBase):
    '''
    classdocs
    '''

    def __init__(self, params):
        '''
        Constructor
        '''

class OptimalTrajectoryLearning(AlgorithmBase):        
    '''
    Assume a full optimal trajectory is known in advance and train only against
    that (no simulation). 
    '''
    
    def __init__(self, dataset, model):
        '''
        Constructor
        '''
        self.dataset = dataset
        self.model = model

    def train_batch(self, batch):
        gradient = 0
        for sentence, output in batch:
            for i in range(len(sentence)):
                gradient += self.model.gradient(sentence, i, output[i])
        
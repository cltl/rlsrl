'''
Created on Jul 13, 2015

@author: Minh Ngoc Le
'''

def evaluate(sents, linkages, predicate_disambiguation=False):
    '''
    Perform CoNLL-2008 style evaluation.
    '''
    gold_total = 0
    predicted_total = 0
    correct = 0
    for sent, links in zip(sents, linkages):
        gold_links = (sent.sem_links if predicate_disambiguation
                      else [l for l in sent.sem_links if l[1] != 0])
        links = (links if predicate_disambiguation
                 else [l for l in links if l[1] != 0])
        correct += len(set(gold_links).intersection(links))
        gold_total += len(gold_links)
        predicted_total += len(links)
    p = float(correct)/predicted_total if predicted_total > 0 else float('nan')
    r = float(correct)/gold_total if gold_total > 0 else float('nan')
    f1 = 2 / (1/p + 1/r) if p > 0 and r > 0 else 0
    return (p, r, f1, correct, gold_total, predicted_total)
    
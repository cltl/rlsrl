This repository contains our experiments on reinforcement learning for semantic
role labeling.

## Reproducing instruction

1. Run `preprocess.py` to create training and test dataset.
2. Run `local.py` to experiment with local model.

## Directories and files

- `bauer2014/` contains Bauer et l. (2012)'s dependency-parsed FrameNet corpus,
more specifically the `fulltext` folder in `basic_dependencies`.
- `data/` is meant to be empty after cloning and will be populated using 
preprocessing script.
- `test/` contains a collection of nose tests. Some tests require preprocessed
data so please make sure that you run `preprocess.py` first.
- `run.job` ignites a job on our Grid Engine, you may want to adapt it to 
your environment 


## References 

- Bauer, D., Fürstenau, H., & Rambow, O. (2012). The Dependency-Parsed FrameNet Corpus. Proceedings of the Eighth International Conference on Language Resources and Evaluation (LREC-2012), 3861–3867. Retrieved from http://www.lrec-conf.org/proceedings/lrec2012/pdf/1037_Paper.pdf